from autobahn.twisted.wamp import ApplicationSession
from twisted.internet.defer import inlineCallbacks

class Authorizer(ApplicationSession):
    """ Авторизация для анонимных пользователей """
    
    @inlineCallbacks
    def onJoin(self, details):
        yield self.register(self.authorize, 'authorize')

    def authorize(self, session, uri, action, options):
        """ Обработчик запросов на авторизацию
            Выполняется каждый раз, когда анонимный пользователь хочет выполнить
            какую-нибудь команду """
        
        # анонимным пользователям запрещено вызывать приватные методы,
        # которые начинаются с "_"
        fname = uri.rsplit('.')[-1]
        if fname.startswith('_') and action == 'call':
            return False
        return {"allow": True, "disclose": True}

