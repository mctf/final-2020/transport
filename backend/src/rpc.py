# Варианты использования
#
# @rpc
# def func(*args, _ctx: Optional):
#   *args - список аргументов функции
#   _ctx - контекст переданный crossbar (сессия вызвавшего и тд и тп)
#   Метод зарегистрируется под именем func и любые пользователи роутера
#   будут иметь возможность вызывать его
#
# @rpc('renamed')
# def func(*args):
#   Функция будет доступна под именем renamed, а не func
#
# @rpc
# def _func(*args):
#   Префиксный _ делает функцию приватной
#   Функцию может вызвать только другой сервис
#   Для понимания работы смотри репо crossbar
#   crossbar.authorizer
#
# res = await rpc.call(name, *args)
# ожидание результата функции
#
# loop.create_task(rpc.call(name, *args))
# забить на ожидание ответа
#
# rpc.call(name, *args, on_progress=progress)
# получение результатов от генератора
#
# rpc.publish(name, *args)
# оповещение всех sub

from os import environ
import asyncio

import time

from autobahn.asyncio.wamp import ApplicationSession, ApplicationRunner
from autobahn.wamp.types import RegisterOptions, CallOptions, PublishOptions
from autobahn.wamp import auth

import inspect
import sys

import logging
logging.basicConfig(level=logging.INFO)

module = sys.modules[__name__]

_funcs = {}
class Mimic(module.__class__):
    """ Маскировка глобальных переменных модуля """
    
    __dict__ = ['call', 'publish', 'run']
    
    def __call__(self, n=None):
        """ Регистрация RPC методов """
        name = None

        def pub(fn):
            if name in _funcs and _funcs[name] != fn:
                raise NameError('Method {} is already defined elsewhere'.format(name))
            _funcs[name] = fn
            return fn

        if isinstance(n, str):
            name = n
            return pub
        elif callable(n):
            name = n.__name__
            return pub(n)
        else:
            raise ValueError('Incorrect @rpc decorator argument')
            
            
module.__class__ = Mimic

class Component(ApplicationSession):
    async def onConnect(self):
        """ Инициация аутентификации сервиса в роутере """
        self.join('realm1', ['wampcra'], environ.get('AUTOBAHN_USER'))

    async def onChallenge(self, challenge):
        """ Аутентификации сервиса в роутере """
        if challenge.method == 'wampcra':
            secret = environ.get('AUTOBAHN_SECRET').encode('utf8')
            challenge_ = challenge.extra['challenge'].encode('utf8')
            signature = auth.compute_wcs(secret, challenge_)
            return signature.decode('ascii')
        else:
            raise Exception("don't know how to handle authmethod {}".format(challenge.method))
            
    async def onJoin(self, details):
        """ Регистрация методов RPC в роутере """
        
        async def call(*args, options={}):
            """ Вызов RPC процедур, зарегистрированных другими воркерами """
            return await self.call(*args, options=CallOptions(**options))
            
        async def publish(*args, options={}):
            """ Публикация сообщений всем подписавшимся """
            return await self.publish(*args, options=PublishOptions(**options))
        
        # функции добавляются в модуль через замыкание для проброса внутрь них
        # контекста self
        setattr(module, 'call', call)
        setattr(module, 'publush', publish)
        
        for name in _funcs:
            func = _funcs.get(name)
            # объявление балансировки нагрузки для роутера
            options = RegisterOptions(invoke='roundrobin')
            if '_ctx' in inspect.signature(func).parameters:
                options.details_arg = '_ctx'
            # методы RPC регистрируются в именном пространстве, который по сути
            # является именем модуля
            await self.register(func, name, options)
            
    def onDisconnect(self):
        """ Выключение воркера """
        # изза специфики работы функции ApplicationRunner.run
        # где происходит loop.run_forever, который при отсутсвии корутин
        # продолжает работать, тем самым не давая воркеру переподключиться
        asyncio.get_event_loop().stop()

def run(url=None, realm='realm1'):
    """ Запуск воркера """
    url = url or environ.get('AUTOBAHN_ROUTER')
    ApplicationRunner(url, realm).run(Component)

