import rpc
import db
from user import get_user
import auth

@rpc('profile.get')
async def get(_ctx):
    with db.db_session:
        await auth.can('profile.get', _ctx)
        user = await get_user(_ctx)
        return user.profile.to_dict()

@rpc('profile.set')
async def set_(attrs, _ctx):
    with db.db_session:
        await auth.can('profile.set', _ctx)
        user = await get_user(_ctx)
        user.profile.set(**attrs)