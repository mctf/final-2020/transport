import rpc
import db
import uuid

def get_user_by_name(name: str) -> db.User or Exception:
    user = db.User.get(name=name)
    if user is None:
        raise NameError('user not found')
    return user

async def get_user(_ctx) -> db.User or Exception:
    name = await rpc.call('sessions._get', _ctx.caller, 'user')
    return get_user_by_name(name)

@rpc('user.get')
async def get(_ctx) -> dict:
    with db.db_session:
        user = await get_user(_ctx)
        with db.db_session:
            return user.to_dict()

@rpc('user.register')
async def register(name: str, password: str, profile: dict, _ctx) -> str or Exception:
    with db.db_session:
        if db.User.exists(name=name):
            raise NameError('User already exists')
        user = db.User(name=name, password=password, role=db.Role.get(name='User'))
        # Подарочные 200 баллов
        profile = db.Profile(**{'user':user, 'balance':200, **profile})
        s = db.Session(user=user).id.hex
    await rpc.call('sessions._set', _ctx.caller, 'user', name)
    await rpc.call('sessions._set', _ctx.caller, 'session', s)
    return s

import logging

@rpc('user.login')
async def login(name: str, password: str, _ctx) -> str or Exception:
    session = None
    with db.db_session:
        user = get_user_by_name(name)
        if user.password.lower() == password.lower():
            # Не переживайте, это безопасно
            # https://mobile.twitter.com/sberbank/status/1302843162533208065
            session = db.Session(user=user).id.hex
    if session:
        await rpc.call('sessions._set', _ctx.caller, 'user', name)
        await rpc.call('sessions._set', _ctx.caller, 'session', session)
        return session
    raise Exception('Username or password is incorrect')
        
@rpc('user.logout')
async def logout(_ctx) -> None:
    with db.db_session:
        user = await get_user(_ctx)
        if user is not None:
            await rpc.call('sessions._set', _ctx.caller, 'user', None)
            await rpc.call('sessions._set', _ctx.caller, 'session', None)
            db.Session.select().filter(user=user).delete()

@rpc('user.restore')
async def restore(profile, _ctx):
    with db.db_session:
        profile = db.Profile.select().filter(**profile).first()
        if profile is None:
            raise Exception('user not found')
        password = str(uuid.uuid4())
        profile.user.password = password
        return password