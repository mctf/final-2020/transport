import { z, page, Ref } from '../2ombular';
import router from '../router';
import * as auth from '../auth';
import RPC from '../rpc';

let modal, error;

const Input = (Val, p) => z._input.c1.b4({
    oninput(e) {
        error = '';
        Val(e.target.value);
        page.update()
    }, value: Val, ...p
});

const Check = (val, text) => z.v.cp.flex.ac.usn({ onclick(e) { val(!val()); page.update(); }}, _=>z.c2['gg-check-'+ (val() ? 'r' : 'n')].ib(), z.v({style: 'margin: .2em'}), text);

let data = {};
const Main = _=> z.main(
    z.f2('Если у вас возникли проблемы с использованием приложения можете обратиться в техподдержку по адресу tc@m_os.ru, наши нейронные сети обработают ваш запрос в ближайший месяц'),
    z.f2('Вы можете стать нашим партнером, ', z._a.c4.cp({
        onclick() {
            modal.style.display = 'flex';
        }
    }, 'зарегистрироваашись'), ', как водитель, подробнее узнайте в ближайшем муниципальном учреждении вашего города'),
    
    z.modal__overlay(
        { key: 'support-modal', on$created(ev) {
            modal = ev.target;
            window.addEventListener('click', e => {
                if (e.target == modal) {
                    modal.style.display = 'none';
                }
            })
        } },
        z.modal__container.f2(
            z.modal__header('Стать партнером'),
            z.modal__content(
                z.f3('Автомобиль'),
                z.sp1(Input(Ref(data, 'car'), {placeholder: 'Наименование модели', required: true})),
                z.sp1(Input(Ref(data, 'passangers'), {placeholder: 'Количество пассажиров', type: 'number', min: 1})),
                z.sp1(Check(Ref(data, 'is_premium'), 'Бизнес класс')),
                z.sp2.f3('Цена'),
                z.sp1(Input(Ref(data, 'tarriff_base'), {placeholder: 'Цена за вызов', type: 'number', min: 50})),
                z.sp1(Input(Ref(data, 'tarriff_per_km'), {placeholder: 'Цена за километр', type: 'number', min: 5})),
                z.sp2.f3('Дополнительные сведения'),
                z.sp1(Input(Ref(data, 'description'), {placeholder: 'Описание'})),
                z.sp1(Input(Ref(data, 'prepaid_comment'), {placeholder: 'Комментарий к предоплаченным вызовам'})),
                _=>z.sp2.c4(error)
            ),
            z.modal__footer(
                z.flex(
                    z.modal__btn.primary({
                        onclick(e) {
                            let c = true;
                            document.querySelectorAll('.modal__content input').forEach(i => {
                                if (!i.checkValidity()) {
                                    i.reportValidity();
                                    c = false;
                                }
                            });
                            if (c) {
                                RPC.session.call('driver.register', [data])
                                .then(res => {
                                    data = {};
                                    auth.get_user()
                                    router.navigate('/driver');
                                })
                                .catch(e => {
                                    error = e.args[0];
                                })
                                .finally(page.update);
                            }
                        }
                    }, 'Зарегистрироваться'),
                    z.modal__btn({style:'margin-left:15px', onclick(e) {
                        data = {};
                        modal.style.display = 'none';
                        page.update();
                    }}, 'Закрыть'))
            )
        )
    )
);

export default Main;