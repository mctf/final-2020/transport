import { z, page, Ref } from '../2ombular';
import * as auth from '../auth';
import RPC from '../rpc';

const Line = (icon, title, val) => z.flex.sp3(z.v.flex.jc.ac({style: '--ggs: 1.7; width: 30px'}, z._i.c2[icon]), z.v({style: 'margin-left: 30px'}, z.c2.f2(title), z.c1.f3(val)))

let modal;

const Input = (Val) => z._input.c1.b4({
    oninput(e) {
        Val(e.target.value);
        page.update()
    }, value: Val
});

let data = {}, c = {};
const Main = _=> z.main(
    z.v({onclick(e) {
        modal.style.display = 'flex';
    }},
    Line('gg-user', 'ФИО', data.name),
    Line('gg-credit-card', 'Кошелек', new Intl.NumberFormat('ru-RU', { style: 'currency', currency: 'RUB' }).format(data.balance)),
    Line('gg-controller', 'Паспорт', data.passport),
    Line('gg-heart', 'СНИЛС', data.insurance),
    Line('gg-smartphone-chip', 'Номер чипа', data.chip)),
    z.modal__overlay(
        { key: 'profile-modal', on$created(ev) {
            modal = ev.target;
            window.addEventListener('click', e => {
                if (e.target == modal) {
                    ev.target.style.display = 'none';
                }
            })
        } },
        z.modal__container.f2(
            z.modal__header('Изменение профиля'),
            z.modal__content(
                z.v('ФИО'),
                Input(Ref(c, 'name')),
                z.sp1('ПАСПОРТ'),
                Input(Ref(c, 'passport')),
                z.sp1('СНИЛС'),
                Input(Ref(c, 'insurance')),
                z.sp1('НОМЕР ЧИПА'),
                Input(Ref(c, 'chip'))
            ),
            z.modal__footer(
                z.flex(
                    z.modal__btn.primary({
                        onclick(e) {
                            RPC.session.call('profile.set', [c])
                            .then(e => {
                                Object.assign(data, c);
                            })
                            .catch(console.error)
                            .finally(_ => {
                                modal.style.display = 'none';
                                page.update();
                            })
                        }
                    }, 'Сохранить'),
                    z.modal__btn({style:'margin-left:15px', onclick(e) {
                        Object.assign(c, data);
                        modal.style.display = 'none';
                        page.update();
                    }}, 'Закрыть'))
            )
        )
    )
);

const get_profile = res =>
res &&
RPC.session.call('profile.get')
    .then(res => {
        data = res;
        Object.assign(c, data);
    })
    .catch(console.error)
    .finally(page.update)

auth.e.on('session.check', get_profile);

export default Main;