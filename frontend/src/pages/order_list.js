import { z, page, Ref } from '../2ombular';
import * as auth from '../auth';
import RPC from '../rpc';

const Titled = (name, c) => z.v(z.f2.c2(name), c)

const numf = new Intl.NumberFormat('ru-RU', { style: 'currency', currency: 'RUB' })

let data = [];
const Main = _=> z.main.f3(
    data.map(i => z.sp1(
        Titled('Карта',
            z._a.c4({ href: `https://www.openstreetmap.org/directions?engine=fossgis_osrm_car&route=${i.from_lat}%2C${i.from_lon}%3B${i.to_lat}%2C${i.to_lon}`}, 'Открыть')
        ),
        z.flex.sp1(
            Titled('Цена', numf.format(i.price)),
            z.v({style: 'margin-left: 30px'}),
            Titled('Предоплата', z.c2['gg-check-'+ (i.prepaid ? 'r' : 'n')].ib())),
        i.gift ? z.sp1(Titled('Комментарий водителя', i.gift)) : ''
    ))
);

const get_list = res =>
res &&
RPC.session.call('order.list')
    .then(res => {
        data = res;
    })
    .catch(console.error)
    .finally(page.update)

auth.e.on('session.check', get_list);

export default Main;