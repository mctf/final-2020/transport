export default class EventEmitter {
    constructor() {
        this._handlers = {}
    }
    on(event, fn) {
        (this._handlers[event] || (this._handlers[event] = new Set())).add(fn);
        return this;
    }
    off(event, fn) {
        this._handlers[event] && this._handlers[event].delete(fn);
        return this;
    }
    emit(event, data) {
        this._handlers[event] && this._handlers[event].forEach(fn=>fn(data));
        return this;
    }
}