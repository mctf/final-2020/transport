import autobahn from 'autobahn-browser';
import EventEmitter from './events';

const connection = new autobahn.Connection({
    url: `${location.protocol === 'http:' ? 'ws' : 'wss'}://${location.host}/api`,
    realm: 'realm1'});

const main = new EventEmitter();

export default main;

connection.onopen = function (session) {
    main.session = session;
    main.emit('open', session);
};

connection.onclose = function() {
    main.emit('close', {});
}

connection.open();