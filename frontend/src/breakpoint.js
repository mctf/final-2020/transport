import { page } from './2ombular';

const breakpoints = {
    xs: {
        min: 0,
        max: 767
    },
    // Small devices (tablets)
    sm: {
        min: 768,
        max: 991
    },
    // Medium devices (desktops)
    md: {
        min: 992,
        max: 1199,
    },
    // Large devices (large desktops)
    lg: {
        min: 1200,
        max: Infinity
    }
}

let breakpoint = getBreakpoint();

function getBreakpoint() {
    return Object.entries(breakpoints).find(([k, v]) => v.min <= window.innerWidth && window.innerWidth <= v.max)[0];
}

window.addEventListener("resize", _ => {
    if (getBreakpoint() != breakpoint)
        page.update();
});