import { z, page } from './2ombular';
import Router from './router';

import './style.css';
import './breakpoint';

import './rpc';
import './pages';

import Auth from './auth';

Router.register('', _=> '');

page.setBody(Auth(Router));
