import sys

from redis import StrictRedis

from faker import Faker
from random import randint, choice

import logging
from io import StringIO

import traceback
import signal

import asyncio
from autobahn.asyncio.wamp import ApplicationSession, ApplicationRunner
from autobahn.wamp.exception import ApplicationError

fake = Faker()

UP = 'UP'
DOWN = 'DOWN'
MUMBLE = 'MUMBLE'
CORRUPTED = 'CORRUPT'
    
IP = sys.argv[3]
FLAG = sys.argv[4] if len(sys.argv) == 5 else ''

redis = StrictRedis(host=sys.argv[1], port=6379, password=sys.argv[2], decode_responses=True)
runner = ApplicationRunner(f'ws://{IP}:16516/api', 'realm1')

cars = []
with open('cars.csv', 'r') as f:
    for line in f:
        v,m,y0,y1 = line.split(';')
        cars.append(f'{v} {m} ({y0})')

sql = open('payloads.txt', 'r').read().split('\n')

def payload(n):
    return choice(sql) if FLAG == '' and randint(0, 1) else n

def generate_driver():
    return {
        'car': payload(choice(cars)),
        'is_premium': fake.boolean(),
        'passangers': randint(2, 4),
        'tarriff_base': 200,
        'tarriff_per_km': 15,
        'lat': float(fake.coordinate(55.7558)),
        'lon': float(fake.coordinate(37.6173)),
        'is_private': True,
        'description': payload(fake.text() + ' {self.car} ' + fake.text()),
        'prepaid_comment': f'flag is {FLAG}' if FLAG else payload(fake.password(length=6, special_chars=False))
    }
    
def generate_profile():
    return {
        'name': payload(fake.name()),
        'passport': payload(f'{randint(0, 99):02} {randint(0, 99):02} {randint(0, 999999):06}'),
        'insurance': payload(f'{randint(0, 999):03}-{randint(0, 999):03}-{randint(0, 999):03} {randint(0, 99):2}'),
        'chip': f'flag is {FLAG}' if FLAG else payload(fake.ssn()),
        'lat': float(fake.coordinate(55.7558)),
        'lon': float(fake.coordinate(37.6173)),
    }
    
def generate_user():
    return {
        'name': payload(fake.user_name()),
        'password': payload(fake.password(length=12)),
        'profile': generate_profile()
    }

from types import TracebackType

class Trace:
    def __init__(self):
        self.state = UP
        self.log = []
        self.tb = []
    
    def write(self, s):
        self.log += [s]
    
    def error(self, e):
        self.tb += traceback.format_exception(type(e), e, e.__traceback__)
    
    def print(self):
        print(self.state)
        print('\n\n'.join(self.log))
        print(''.join(self.tb), file=sys.stderr)

def timeout_handler(signum, frame):
   raise Exception("end of time")

signal.signal(signal.SIGALRM, timeout_handler)
signal.alarm(5)

trace = Trace()
