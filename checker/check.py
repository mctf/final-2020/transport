from shared import *

USER_PERMISSIONS = ['auth.can', 'profile.set', 'profile.get', 'transfer', 'referal.sign', 'referal.create', 'referal.get', 'order.create', 'order.list', 'order.get', 'driver.random', 'driver.list', 'driver.register']
DRIVER_PERMISSIONS = [*USER_PERMISSIONS, 'driver.get', 'driver.set']

tests = [
    'test_register',
    'test_session_login',
    'test_session_invalid',
    'test_logout',
    'test_user_restore',
    'test_login',
    'test_user_get',
    'test_profile_get',
    'test_profile_set',
    'test_user_auth_can',
    'test_transfer',
    'test_analytics',
    'test_driver_register',
    'test_driver_auth_can',
    'test_driver_get',
    'test_driver_set',
    'test_driver_random',
    'test_driver_list',
    'test_order_create',
    'test_list_orders'
]

from collections import namedtuple

class Component(ApplicationSession):
    async def test_register(self):
        """ Регистрация пользователя """
        self.session = await self.call('user.register', **self.user)
    
    async def test_session_login(self):
        """ Вход через ключ сессии """
        if not await self.call('session.check', self.session):
            raise Exception('Ключ сессии, выданный сервисом после регистрации, неверный')
    
    async def test_session_invalid(self):
        """ Проверка неправильной сессии """
        if await self.call('session.check', fake.md5()):
            raise Exception('Неверный ключ был подтвержлен')
    
    async def test_logout(self):
        """ Проверка выхода """
        await self.call('user.logout')

    async def test_user_restore(self):
        """ Проверка восстановления пользователя """
        self.user['password'] = await self.call('user.restore', self.user['profile'])
            
    async def test_login(self):
        """ Проверка входа """
        self.session = await self.call('user.login', self.user['name'], self.user['password'])
    
    async def test_user_get(self):
        """ Проверка получения своего пользователя """
        user = await self.call('user.get')
        assert user['name'] == self.user['name']
    
    async def test_profile_get(self):
        """ Проверка Получения профиля """
        profile = await self.call('profile.get')
        #trace.tb += [f'{self.user["profile"]}\n{profile}\n']
        assert self.user['profile']['name'] == profile['name']
        assert self.user['profile']['passport'] == profile['passport']
        assert self.user['profile']['insurance'] == profile['insurance']
        assert self.user['profile']['chip'] == profile['chip']
        assert 200 == profile['balance']
  
    async def test_profile_set(self):
        """ Проверка изменения профиля """
        self.user['profile'] = generate_profile()
        await self.call('profile.set', self.user['profile'])
        await self.test_profile_get()
        
    async def test_user_auth_can(self):
        """ Проверка прав пользователя """
        for i in USER_PERMISSIONS:
            await self.call('auth.can', i)
    
    async def test_transfer(self):
        """ Проверка переводов между пользователями """
        u = self.user
        self.user = generate_user()
        await self.test_register()
        await self.call('transfer', u['name'], 200)
        self.user = u
        await self.test_login()
        profile = await self.call('profile.get')
        assert profile['balance'] == 400
    
    async def test_analytics(self):
        """ Проверка аналитик """
        try:
            await self.call('analytics.sql', 'select * from orders')
        except Exception as e:
            assert e.args[0] == '"analytics.sql" denied for "User"'
    
    async def test_driver_register(self):
        """ Регистрация водителя """
        self.driver = generate_driver()
        await self.call('driver.register', self.driver)
    
    
    async def test_driver_auth_can(self):
        """ Проверка прав таксиста """
        for i in DRIVER_PERMISSIONS:
            await self.call('auth.can', i)

    async def test_driver_get(self):
        """ Проверка получения данных водителя """
        driver = await self.call('driver.get')
        #trace.tb += [f'{self.driver}\n{driver}\n']
        assert driver['car'] == self.driver['car']
        assert driver['is_premium'] == self.driver['is_premium']
        assert driver['passangers'] == self.driver['passangers']
        assert driver['tarriff_base'] == self.driver['tarriff_base']
        assert driver['tarriff_per_km'] == self.driver['tarriff_per_km']
        assert driver['lat'] == self.driver['lat']
        assert driver['lon'] == self.driver['lon']
        assert driver['description'] == self.driver['description']
        assert driver['prepaid_comment'] == self.driver['prepaid_comment']
    
    async def test_driver_set(self):
        """ Проверка ввода данных водителя """
        self.driver = generate_driver()
        await self.call('driver.set', self.driver)
        await self.test_driver_get()
    
    def _test_public_dict(self, driver):
        assert driver['username'] == self.user['name']
        assert driver['name'] == self.user['profile']['name']
        assert driver['car'] == self.driver['car']
        assert driver['is_premium'] == self.driver['is_premium']
        assert driver['description'] == self.driver['description'].format(self=namedtuple('Driver', self.driver.keys())(**self.driver))
        assert driver['lat'] == self.driver['lat']
        assert driver['lon'] == self.driver['lon']

    async def test_driver_random(self):
        """ Получения рандомного водителя """
        driver = await self.call('driver.random', self.driver)
        self._test_public_dict(driver)
    
    async def test_driver_list(self):
        """ Получение списка водителей """
        driver = await self.call('driver.list', self.driver)
        self._test_public_dict(driver[0])
    
    async def test_order_create(self):
        """ Вызов таксиста """
        driver = await self.call('driver.random', self.driver)
        order_id = await self.call('order.create', driver['id'], [self.user['profile']['lat'], self.user['profile']['lon']], True)
        order = await self.call('order.get', order_id)
        assert order['gift'] == self.driver['prepaid_comment']

    async def test_list_orders(self):
        """ Получение списка вызовов """
        assert len(await self.call('order.list')) == 1

    async def onJoin(self, details):
        try:
            any_ = False
            self.user = generate_user()
            for t in tests:
                t = getattr(self, t)
                ok = False
                error = ''
                try:
                    await t()
                    ok = True
                    any_ = True
                except ApplicationError as e:
                    error = e.args[0] if len(e.args) else ''
                except Exception as e:
                    error = str(e)
                    trace.error(e)
                trace.tb += [f'{t.__doc__.strip()} {"NOT " if not ok else ""}OK\n{error}']
                if not ok:
                    trace.state = MUMBLE
                    trace.write(f'Тест <{t.__doc__.strip()}> не пройден ({error})')
                    break
                
        except Exception as e:
            trace.state = DOWN
            trace.error(e)
        if not any_:
            trace.state = DOWN
        self.leave()

    def onDisconnect(self):
        asyncio.get_event_loop().stop()

if __name__ == '__main__':
    try:
        runner.run(Component)
    except Exception as e:
        trace.state = DOWN
        trace.write('Не удалось подключиться')
        trace.error(e)
    trace.print()

